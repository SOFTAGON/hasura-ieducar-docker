FROM nginx:alpine AS nginx
#
FROM php:8.1.8-fpm-alpine

LABEL maintainer="Softagon Sistemas <fale@softagon.com.br>"

ENV COMPOSER_ALLOW_SUPERUSER 1
ENV COMPOSER_PROCESS_TIMEOUT 900
ENV COMPOSER_DISABLE_XDEBUG_WARN=1

ENV XDEBUG_IDEKEY xdebug
ENV XDEBUG_CLIENT_HOST 127.0.0.1
ENV XDEBUG_CLIENT_PORT 9003
ENV XDEBUG_MODE off
ENV XDEBUG_START_WITH_REQUEST off

RUN apk update
RUN apk add wget

RUN apk add --no-cache --virtual .phpize_deps $PHPIZE_DEPS

RUN pecl install xdebug
RUN docker-php-ext-enable xdebug

RUN apk --no-cache add postgresql-dev
RUN docker-php-ext-install pgsql

RUN docker-php-ext-install pdo
RUN docker-php-ext-install pdo_pgsql

RUN pecl install redis
RUN docker-php-ext-enable redis
RUN rm -rf /tmp/pear

RUN docker-php-ext-install bcmath

RUN apk add unzip

RUN apk add libzip-dev
RUN docker-php-ext-install zip

RUN apk add libc-dev

RUN echo "@v3.4 http://nl.alpinelinux.org/alpine/v3.4/main" >> /etc/apk/repositories && \
    apk update && \
    apk add --no-cache "postgresql-client@v3.4>=9.5"

RUN apk add postgresql-client

RUN apk add libpng-dev
RUN docker-php-ext-install gd

RUN apk add jq git

RUN ln -s /var/www/ieducar/artisan /usr/local/bin/artisan

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

COPY xdebug.ini /usr/local/etc/php/conf.d/xdebug.ini

RUN docker-php-ext-install pcntl

RUN apk add openjdk8
RUN apk add ttf-dejavu

RUN apk add --update npm

RUN composer self-update

RUN apk del .phpize_deps

# horário do Brasil
RUN apk update && apk add tzdata
ENV TZ="America/Recife"
RUN date

# i-educar
ENV IEDUCAR_ROOT=/var/www/ieducar
ARG IEDUCAR_VERSION=2.7.4
RUN wget https://github.com/portabilis/i-educar/archive/refs/tags/${IEDUCAR_VERSION}.tar.gz -O /tmp/i-educar.tar.gz
COPY softagon.php uploads.ini reports.sh ieducar-patch.tar.gz /tmp/

# NGINX
RUN apk add --no-cache nginx

RUN rm -rf /etc/nginx

COPY --from=nginx /docker-entrypoint.d /docker-entrypoint.d
COPY --from=nginx /docker-entrypoint.sh /docker-entrypoint.sh
COPY --from=nginx /etc/nginx /etc/nginx

COPY default.conf /etc/nginx/conf.d/default.conf
COPY 99-ieducar.sh /docker-entrypoint.d/

RUN chmod +x /docker-entrypoint.d/*.sh

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["nginx","-g","daemon off;"]

#!/bin/sh
## Shell Script para instalar portabilis/i-educar-reports-package.git
## 20/04/2022 11:45 AM
echo "#####|> Iniciando a instalação <|#####"
echo "Rode este script dentro de /var/www/ieducar/"
echo "1. Copiando do github..."
cd /var/www/ieducar && rm -rf packages/portabilis/i-educar-reports-package
 git clone https://github.com/portabilis/i-educar-reports-package.git packages/portabilis/i-educar-reports-package
rm -Rf ieducar/modules/Reports
echo "2. Composer na versão obrigatória..."
composer self-update --2.2
echo "3. Instalando junto ao ieducar..."
composer plug-and-play:update --no-interaction
php artisan legacy:link
php artisan community:reports:link
php artisan reports:install
php artisan storage:link
echo "Finalizado, comente algo em: https://gist.github.com/hermesalvesbr/64087fb185e86802d316a254c669cdc6"
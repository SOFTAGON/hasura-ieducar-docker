#!/bin/sh
set -e

# FIRST INSTALL
FIRST_INSTALL=0
if [ -e ${IEDUCAR_ROOT} ]; then
 [ "$(ls -A ${IEDUCAR_ROOT})" = "" ] && FIRST_INSTALL=1 || true
else
 FIRST_INSTALL=1
fi

function first_install(){
 mkdir -p ${IEDUCAR_ROOT}
 tar --strip-components=1 -zxf /tmp/i-educar.tar.gz -C ${IEDUCAR_ROOT}
 cd ${IEDUCAR_ROOT}
 ln -sf /usr/local/bin/composer .
 npm install -g yarn
 php composer new-install

 #patch 2.7.4
 tar -xvf /tmp/ieducar-patch.tar.gz -C ${IEDUCAR_ROOT}

 #reports
 cp /tmp/reports.sh ${IEDUCAR_ROOT}
 chmod +x ${IEDUCAR_ROOT}/reports.sh
 cd ${IEDUCAR_ROOT} && ./reports.sh > /dev/null 2>&1

 #uploads conf
 cp /tmp/uploads.ini /usr/local/etc/php/conf.d/
 cp /tmp/softagon.php /var/www/ieducar/ieducar/intranet/


}

if [ $FIRST_INSTALL -eq 1 ]; then first_install; else echo "Directory ${IEDUCAR_ROOT} NOT EMPTY!!!"; fi

echo "LOADING PHP-FPM"
echo "AGUARDE 90 SEGUNDOS - $(date +'%m/%d/%Y  %H:%M:%S')"
echo "softagon sistemas"
docker-php-entrypoint php-fpm &
